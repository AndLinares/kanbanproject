require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});
const express = require("express");
const passport = require("passport");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const path = require("path");
const mongoose = require("mongoose");
var flash = require("connect-flash");
const LocalStrategy = require("passport-local").Strategy;
const appRoutes = require("./controllers/App.routes");
const routes = require("./controllers/get.routes");
const routesPost = require("./controllers/post.routes");

const { findUser } = require("./models/createUser");

const app = express();

/* MiddleWare */
app.set("views", "./src/views");
app.set("view engine", "ejs");
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.set("port", process.env.PORT);
app.use(cookieParser(process.env.SECRET));
app.use(
  session({
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: true,
  })
);

/* PASSPORT STRATEGY */
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
passport.use(
  new LocalStrategy(async (username, password, done) => {
    await mongoose.connect(process.env.URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    let db = mongoose.connection;
    let user = await findUser({ name: username });
    // Why i can join with any user and password register in db
    if (user != null) {
      if (password == user.password) db.close();
      return done(null, user);
    }
    db.close();
    done(null, false);
  })
);

passport.serializeUser((user, done) => {
  done(null, user._id);
});

passport.deserializeUser(async (id, done) => {
  await mongoose.connect(process.env.URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  let db = mongoose.connection;
  let user = await findUser({ _id: id });
  done(null, user);
});

/* ROUTES */
app.use(express.static(path.join(__dirname, "public")));
app.use("/App/", routesPost);
app.use("/App/", appRoutes);
app.use("/", routes);

app.post(
  "/login",
  passport.authenticate("local", {
    successRedirect: "/App",
    failureRedirect: "/failure",
    failureFlash: true,
  })
);

app.listen(3001, "0.0.0.0", () => {
  console.log("Runing in port 3001");
});

module.exports = { app };
