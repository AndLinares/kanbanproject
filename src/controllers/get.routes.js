var express = require('express');
var router = express.Router();
var mongoose = require('mongoose')
var {findTask} = require('../models/createTask')
var messageText = '';

router.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

async function organizeTask(a, b) {
  console.log('hello');
  return b.date - a.date
}

router.get('/App', async (req, res)=>{
  messageText = ''
  if(req.isAuthenticated()){
    await mongoose.connect(process.env.URI, {useNewUrlParser: true, useUnifiedTopology: true});
    let db = mongoose.connection;
    let id = req.user.email;
    let cards = await findTask({idUser: id})
    cards.sort(function(a, b) {
      if (a.date > b.date) return 1;
      else if (a.date < b.date) return -1;
      else return 0;
  })
    // console.log(cards);
    res.render('pages/inicio', {card: cards})
  }else{
    res.redirect('/')
  }
})

router.get('/failure', (req, res) => {
  res.status(401)
  messageText = 'Invalid User or Password'
  res.redirect('/')
})

router.get('/', (req, res)=>{
  if(req.isAuthenticated()){
    res.redirect('/App')
  }else{
    res.render('pages/login', {message: messageText})
  }
})

router.get('*', function(req, res){
  res.redirect('/')
});

module.exports = router;