var express = require('express');
var router = express.Router();

async function isAuth(req,res, next) {
  if(req.isAuthenticated()){
    next()
  }else{
      res.redirect('/')
  }
}
 
router.all('/*', isAuth,(req, res, next) => {
  next()
})
  
router.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});
  
router.get('/newTask', (req, res)=>{
    res.render('pages/newTask')
})

module.exports = router;