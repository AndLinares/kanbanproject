var express = require('express');
var router = express.Router();
var mongoose = require('mongoose')
var {findTask, createTask, updateTask, deleteTask} = require('../models/createTask')

async function isAuth(req,res, next) {
  if(req.isAuthenticated()){
    next()
  }else{
      res.redirect('/')
  }
}

router.all('/*', isAuth,(req, res, next) => {
  next()
})

router.post('/sendTask', async (req, res)=>{
  if (req.body.cancel == 'true'){
    res.redirect('/App')
  }else{
    await mongoose.connect(process.env.URI, {useNewUrlParser: true, useUnifiedTopology: true});
    let db = mongoose.connection;
    let newTask = {
      name: req.body.name,
      date: req.body.date,
      description: req.body.description,
      state: 'ToDo',
      idUser: req.user.email
    }
    createTask(newTask)
    res.redirect('/App')
  }
})

router.post('/updateTask', async (req, res)=>{
  if (req.body.cancel == 'true'){
    res.redirect('/App')
  }else{
    await mongoose.connect(process.env.URI, {useNewUrlParser: true, useUnifiedTopology: true});
    let db = mongoose.connection;
    let newTask = {
      name: req.body.name,
      date: req.body.date,
      description: req.body.description,
      state: req.body.state,
      idUser: req.user.email
    }
    await updateTask({_id: req.body.idTask},newTask)
    res.redirect('/App')
  }
})

router.post('/state', async (req, res)=>{
  await mongoose.connect(process.env.URI, {useNewUrlParser: true, useUnifiedTopology: true});
  let db = mongoose.connection;
  await updateTask({_id: req.body.idTask},{state: req.body.stateTask})
  res.redirect('/App')
})

router.post('/edit',async (req, res)=>{
  await mongoose.connect(process.env.URI, {useNewUrlParser: true, useUnifiedTopology: true});
  let db = mongoose.connection;
  if(req.body.edit == 'true'){
    let task = await findTask({_id: req.body.idTask})
    res.render('pages/updateTask', {task: task[0]})
  }else{
    await deleteTask({_id: req.body.idTask})
    res.redirect('/App')
  }
})

module.exports = router;