var card = document.getElementsByClassName('Cards');
let container = document.querySelector('#doneContent')
let container2 = document.querySelector('#inProgressContent')
let container3 = document.querySelector('#toDoContent')
var draged;

for(i in card){
    let cir = card[i]
    if(typeof cir.style != 'undefined'){
        cir.addEventListener('dragstart', dragIniciado)
        cir.addEventListener('dragend', dragFinalizado)
    }
}

container.addEventListener('dragover', dragSobreContainer)
container.addEventListener('dragleave', dragSalioContainer)
container.addEventListener('drop', manejarDrop)
container2.addEventListener('dragover', dragSobreContainer)
container2.addEventListener('dragleave', dragSalioContainer)
container2.addEventListener('drop', manejarDrop)
container3.addEventListener('dragover', dragSobreContainer)
container3.addEventListener('dragleave', dragSalioContainer)
container3.addEventListener('drop', manejarDrop)

function dragIniciado (e) {
    draged = this;
    var padre = document.createElement('p');
    var clone = this.cloneNode(true);
    padre.appendChild(clone);
    e.dataTransfer.setData('text', padre.innerHTML)
}

function dragFinalizado (e) {
}

function dragSobreContainer(e) {
    e.preventDefault();
    return false;
}

function dragSalioContainer(e) {
    e.preventDefault();
    return false;
}

function manejarDrop(e) {
    e.preventDefault();
    let datos = e.dataTransfer.getData('text');
    let changeClass = isClass(this.id, datos)
    this.innerHTML += isState(this.id, changeClass);
    draged.parentNode.removeChild(draged)
    let id = draged.childNodes[7].childNodes[3].childNodes[1].value;
    $(`#${'CardForm' + id}`).trigger('submit');
    for(i in card){
        let cir = card[i]
        if(typeof cir.style != 'undefined'){
            cir.addEventListener('dragstart', dragIniciado, false)
            cir.addEventListener('dragend', dragFinalizado, false)
        }
    }
}

function isClass(id, datos) {
    var changeClass;
    if(id == 'inProgressContent'){
        if(datos.includes('ToDo')){
            changeClass = datos.replace('ToDo', 'InProgress')
        }else{
            changeClass = datos.replace('Done', 'InProgress')
        }
    }else if(id == 'doneContent'){
        if(datos.includes('InProgress')){
            changeClass = datos.replace('InProgress', 'Done')
        }else{
            changeClass = datos.replace('ToDo', 'Done')
        }
    }else if(id == 'toDoContent'){
        if(datos.includes('Done')){
            changeClass = datos.replace('Done', 'ToDo')
        }else{
            changeClass = datos.replace('InProgress', 'ToDo')
        }
    }
    return changeClass;
}

function isState(id, datos) {
    var changeState;
    if(id == 'inProgressContent'){
        if(datos.includes('value="ToDo"')){
            changeState = datos.replace('value="ToDo"', 'value="InProgress"')
        }else{
            changeState = datos.replace('value="Done"', 'value="InProgress"')
        }
    }else if(id == 'doneContent'){
        if(datos.includes('value="InProgress"')){
            changeState = datos.replace('value="InProgress"', 'value="Done"')
        }else{
            changeState = datos.replace('value="ToDo"', 'value="Done"')
        }
    }else if(id == 'toDoContent'){
        if(datos.includes('value="Done"')){
            changeState = datos.replace('value="Done"', 'value="ToDo"')
        }else{
            changeState = datos.replace('value="InProgress"', 'value="ToDo"')
        }
    }
    return changeState;
}
