async function voidTask(e) {
    e.preventDefault()
    let name = document.getElementsByName('name')
    let date = document.getElementsByName('date')
    let description = document.getElementsByName('description')
    if(name[0].value == '' || date[0].value == '' || description[0].value == ''){
        await Swal
        .fire({
            title: `Void fields`,
            text: "some field is void, please don't void any field",
            icon: 'warning',
            confirmButtonText: "OK"
        })
    }else{
        $(`#newForm`).trigger('submit', {});
    }
}