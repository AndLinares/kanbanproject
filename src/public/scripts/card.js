async function deleteTask(e, id, name) {
    e.preventDefault()
    await Swal
    .fire({
        title: `Tarea ${name}`,
        text: "¿Desea Eliminar la tarea?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: "Sí, eliminar",
        cancelButtonText: "Cancelar",
    })
    .then(resultado => {
        if(resultado.value){
            $(`#${'cardForm' + id}`).trigger('submit', {});
        }
    });    
}