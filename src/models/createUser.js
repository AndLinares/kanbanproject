var UserModel = require("./schemas/schemaUser");

async function deleteUser(query) {
  await UserModel.remove(query);
  return true;
}

async function createUser(User) {
  let newUser = new UserModel(User);
  await newUser.save();
  return true;
}

async function updateUser(query, newData) {
  let newUser = {
    $set: newData,
  };
  await UserModel.updateOne(query, newUser);
  return true;
}

async function findUser(query) {
  let user = await UserModel.findOne(query);
  return user;
}

module.exports = {
  createUser,
  findUser,
  updateUser,
  deleteUser,
};
