var TaskModel = require('./schemas/schemaTask')

async function createTask(task){
    let newTask = new TaskModel(task)
    await newTask.save()
    return true
}

async function updateTask(query, newElements) {
    let newValues = {
        $set : newElements
    }
    await TaskModel.updateMany(query, newValues);
    return true
}

async function findTask(query){
    let results = await TaskModel.find(query);
    return results
}

async function deleteTask(query){
    await TaskModel.deleteMany(query);
    return true
}

module.exports = {
    createTask,
    updateTask,
    findTask,
    deleteTask
}

