var {Schema, model} = require('mongoose')

var taskSchema = new Schema({
    name: {type: String, required: true},
    date: {type: Date, required: true},
    description: {type: String, required: true},
    state: {type: String, required: true},
    idUser: {type: String, required:true}
});

module.exports = model('task', taskSchema);
