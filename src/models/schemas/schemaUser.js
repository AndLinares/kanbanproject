var {Schema, model} = require('mongoose')

var UserSchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true, unique:true },
    password: { type: String, required: true }
})

module.exports = model('user', UserSchema);
  